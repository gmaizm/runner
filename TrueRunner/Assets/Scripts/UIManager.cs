using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    int punts;
    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
        StartCoroutine(morePoints());
    }

    IEnumerator morePoints()
    {
        while (true)
        {
            this.punts += 1;
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Points: " + punts;
            yield return new WaitForSeconds(1);
        }


    }
}
