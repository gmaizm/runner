using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeardlyController : MonoBehaviour
{
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player.OnContact += Moverse;
    }

    void Update()
    {
        if (this.gameObject.transform.position.x > -5.5)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.gameObject.GetComponent<Transform>().position = new Vector3((float)-5.6, this.gameObject.transform.position.y);
        }
        if (this.gameObject.transform.position.x < -8.5)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.gameObject.GetComponent<Transform>().position = new Vector3((float)-8.4, this.gameObject.transform.position.y);
        }
    }

    private void Moverse(int num)
    {
        if (num == 0)
        {
            print("zero");
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
        } else if (num == 1)
        {
            print("uuuuu");
            this.gameObject.GetComponent<Animator>().enabled = true;
            StartCoroutine(CanviEscena());
        } else if (num == -1)
        {
            print("enrere!!");
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
        }
        
    }

    IEnumerator CanviEscena()
    {
        yield return new WaitForSeconds(1);
        Destroy(player.gameObject);
        SceneManager.LoadScene("GameOver");
    }
}
