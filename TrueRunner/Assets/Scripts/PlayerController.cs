using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public bool aterriza = false;

    public delegate void Contact(int num);
    public event Contact OnContact;

    public int collisions = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.W) && aterriza)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 600));
            aterriza = false;
        }
        if (Input.GetKeyDown(KeyCode.S) && !aterriza)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -800));
        }
        if (Input.GetKeyDown(KeyCode.S) && aterriza)
        {
            this.gameObject.transform.localScale = new Vector3((float)0.8, (float)0.4, 1);
            this.gameObject.transform.position = new Vector3((float)-2.56, (float)-3.36, 0);
        } 
        if (Input.GetKeyUp(KeyCode.S) && aterriza)
        {
            this.gameObject.transform.localScale = new Vector3((float)0.8, (float)0.8, 1);
            this.gameObject.transform.position = new Vector3((float)-2.56, (float)-2.65, 0);
        }

        //else
        //{
        //this.gameObject.transform.localScale = new Vector3((float)0.8, (float)0.8, 1);
        //this.gameObject.transform.position = new Vector3((float)-2.56, (float)-2.65, 0);
        //}
    }
    IEnumerator Espera5secs()
    {
        yield return new WaitForSeconds(5);
        this.collisions -= 1;
        if (OnContact != null)
            OnContact.Invoke(-1);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "suelo")
        {
            aterriza = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "obstacle")
        {
            if (OnContact != null)
                OnContact.Invoke(collisions);
            this.collisions += 1;
            StartCoroutine(Espera5secs());
        }
    }

    

}
