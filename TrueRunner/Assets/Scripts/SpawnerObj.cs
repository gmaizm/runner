using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerObj : MonoBehaviour
{
    public GameObject[] obstaculos;
    public float vel;
    private int secs = 3;
    void Start()
    {
        StartCoroutine(moreEnemies());
        StartCoroutine(moreVelocity());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator moreEnemies()
    {
        while (true)
        {
            GameObject newenemy = Instantiate(obstaculos[Random.Range(0, obstaculos.Length)]);
            newenemy.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
            yield return new WaitForSeconds(secs);
        }


    }
    IEnumerator moreVelocity()
    {
        while (true)
        {
            vel += 1;
            if (vel == 10)
            {
                secs -= 1;
            }
            if (vel == 15)
            {
                secs -= 1;
            }
            yield return new WaitForSeconds(10);
        }


    }
}
